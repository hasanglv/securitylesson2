package org.example.auth.jwt.rest;

import lombok.RequiredArgsConstructor;
import org.example.auth.jwt.JwtService;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jwt-api")
@RequiredArgsConstructor
public class JwtApi {
    private final JwtService jwtService;
    @GetMapping
    public String issueToken(Authentication authentication){
           return jwtService.issueToken(authentication);
    }
}
