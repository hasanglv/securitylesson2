package org.example;

import lombok.RequiredArgsConstructor;
import org.example.auth.jwt.domain.UserAuthority;
import org.example.auth.jwt.domain.UserEntity;
import org.example.auth.jwt.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityModuleApplication  implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


    public static void main(String[] args) {
        SpringApplication.run(SecurityModuleApplication.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        UserAuthority roleAdmin =
				UserAuthority.builder()
						.authority("ROLE_ADMIN")
						.build();

		UserEntity admin =
				UserEntity.builder()
						.username("admin")
						.password(passwordEncoder.encode("1234"))
						.authorities(List.of(roleAdmin))
						.enabled(true)
						.accountNonExpired(true)
						.credentialsNonExpired(true)
						.accountNonLocked(true)
						.build();

		roleAdmin.setUser(admin);
		Optional<UserEntity> byUsername = userRepository.findByUsername(admin.getUsername());
		if (byUsername.isEmpty()){
			userRepository.save(admin);
		}

		UserAuthority roleAdmin1 =
				UserAuthority.builder()
						.authority("ROLE_ADMIN")
						.build();

		UserEntity atilla =
				UserEntity.builder()
						.username("atilla")
						.password(passwordEncoder.encode("1234"))
						.authorities(List.of(roleAdmin1))
						.enabled(true)
						.accountNonExpired(true)
						.credentialsNonExpired(true)
						.accountNonLocked(true)
						.build();

		roleAdmin1.setUser(atilla);

		Optional<UserEntity> byUsername1 = userRepository.findByUsername(atilla.getUsername());
		if (byUsername1.isEmpty()){
			userRepository.save(atilla);
		}
    }
}



