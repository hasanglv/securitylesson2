package org.example.auth.jwt.configuration;

import lombok.RequiredArgsConstructor;
import org.example.auth.jwt.config.JwtAuthFilterConfigurerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


//    @Bean
//    InMemoryUserDetailsManager inMemoryUserDetailsManager() {
//        String generatedPassword = "{noop}1234";
//        return new InMemoryUserDetailsManager(
//                User.withUsername("user")
//                        .password(generatedPassword)
//                        .roles("USER")
//                        .build(),
//                User.withUsername("admin")
//                        .password(generatedPassword)
//                        .roles("ADMIN")
//                        .build());
//    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeHttpRequests((authorize) -> authorize
                        .requestMatchers("/public").permitAll()
                        .requestMatchers("/role-user").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                        .requestMatchers("/role-admin").hasAuthority("ROLE_ADMIN")
                        .anyRequest()
                        .authenticated())
                .formLogin(Customizer.withDefaults());
        httpSecurity.apply(jwtAuthFilterConfigurerAdapter);
        return httpSecurity.build();
    }
}
