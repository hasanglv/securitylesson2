package com.example.SecurityDemo1;

import com.example.SecurityDemo1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityDemo1Application implements CommandLineRunner {
	private final PasswordEncoder passwordEncoder;
	private final UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(SecurityDemo1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		UserAuthority roleAdmin =
//				UserAuthority.builder()
//						.authority("ROLE_ADMIN")
//						.build();
//
//		UserEntity admin =
//				UserEntity.builder()
//						.username("admin")
//						.password(passwordEncoder.encode("1234"))
//						.authorities(List.of(roleAdmin))
//						.enabled(true)
//						.accountNonExpired(true)
//						.credentialsNonExpired(true)
//						.accountNonLocked(true)
//						.build();
//
//		roleAdmin.setUser(admin);
//		Optional<UserEntity> byUsername = userRepository.findByUsername(admin.getUsername());
//		if (byUsername.isEmpty()){
//			userRepository.save(admin);
//		}
//
//		UserAuthority roleAdmin1 =
//				UserAuthority.builder()
//						.authority("ROLE_ADMIN")
//						.build();
//
//		UserEntity atilla =
//				UserEntity.builder()
//						.username("atilla")
//						.password(passwordEncoder.encode("1234"))
//						.authorities(List.of(roleAdmin1))
//						.enabled(true)
//						.accountNonExpired(true)
//						.credentialsNonExpired(true)
//						.accountNonLocked(true)
//						.build();
//
//		roleAdmin1.setUser(atilla);
//
//		Optional<UserEntity> byUsername1 = userRepository.findByUsername(atilla.getUsername());
//		if (byUsername1.isEmpty()){
//			userRepository.save(atilla);
//		}
	}
}
