package com.example.SecurityDemo1.repository;

import com.example.SecurityDemo1.domain.UserEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @EntityGraph(attributePaths = UserEntity.Fields.authorities)
    Optional<UserEntity> findByUsername(String username);



}
