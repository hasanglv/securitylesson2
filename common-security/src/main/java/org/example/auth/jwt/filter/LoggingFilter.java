package org.example.auth.jwt.filter;

import jakarta.servlet.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
@Component
@Slf4j
@Order(1)
public class LoggingFilter  implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("logging filter is started");
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("Logging Filter in action");
        filterChain.doFilter(servletRequest,servletResponse);

    }

    @Override
    public void destroy() {
        Filter.super.destroy();
        log.info("Logging Filter was destroyed");

    }
}
