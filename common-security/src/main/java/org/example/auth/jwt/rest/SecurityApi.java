package org.example.auth.jwt.rest;


import lombok.RequiredArgsConstructor;
import org.example.auth.jwt.service.UserServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping
@RestController
@RequiredArgsConstructor
public class SecurityApi {
    private final UserServiceImpl userService;
    @GetMapping("/public")
    public String getPublic() {

        return "Hello world,public api";
    }

    @GetMapping("/authenticated")
    public String getAuthenticated() {
        return "Hello world,authenticated api";
    }

    @GetMapping("/role-user")
    public String getRoleUser() {
        return "Hello world,authenticated and requires user role";
    }


    @GetMapping("/role-admin")
    public String getRoleAdmin() {
        return "Hello world,authenticated and requires admin role";
    }

    @GetMapping("/pre-auth/{id}")
    public String sayHello(@PathVariable Integer id, Authentication authentication){

        System.out.println(authentication);
        System.out.println(authentication.getCredentials());
        System.out.println(authentication.getAuthorities());
        System.out.println(authentication.getDetails());
        System.out.println(authentication.getPrincipal());
        System.out.println(authentication.isAuthenticated());

        return userService.sayhello(id);
    }
}