package org.example.auth.jwt.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.auth.jwt.JwtService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;



@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthRequestFilter extends OncePerRequestFilter {

    public static final String AUTHORITIES_CLAIM = "authorities";
    public static final String BEARER = "Bearer";
    public static final String AUTH_HEADER = "Authorization";
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        Optional<Authentication> authOptional = Optional.empty();
        log.info("Our jwt auth filter in action {}",httpServletRequest.getHeader(AUTH_HEADER));
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

//    private Optional<Authentication> authenticate(String authHeader) {
//        final Optional<String> bearer = getBearer(authHeader);
//        if (bearer.isEmpty()) {
//            return Optional.empty();
//        }
//
//        final Claims claims = jwtService.parseToken(bearer.get());
//        final Collection<? extends GrantedAuthority> userAuthorities = getUserAuthorities(claims);
//        final UsernamePasswordAuthenticationToken authenticationToken =
//                new UsernamePasswordAuthenticationToken(claims.getSubject(), "", userAuthorities);
//        return Optional.of(authenticationToken);
//    }
//
//    private Optional<String> getBearer(String header) {
//        if (header == null || !header.startsWith(BEARER)) {
//            return Optional.empty();
//        }
//
//        final String jwt = header.substring(BEARER.length())
//                .trim();
//        return Optional.ofNullable(jwt);
//    }
//
//    private Collection<? extends GrantedAuthority> getUserAuthorities(Claims claims) {
//        List<?> roles = claims.get(AUTHORITIES_CLAIM, List.class);
//        return roles
//                .stream()
//                .map(a -> new SimpleGrantedAuthority(a.toString()))
//                .collect(Collectors.toList());
//    }


}

